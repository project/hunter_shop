<?php

/**
 * @file
 * Contains hunter test class.
 */

namespace Drupal\hunter_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\user\UserAuth;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Fwolf\Wrapper\PHPMailer\PHPMailer;
use Gabrieljmj\Cart\Product\Product;
use Gabrieljmj\Cart\Cart;

/**
 * An example controller.
 */
class HunterTestController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function home(Request $request) {
    $orderby = $request->query->get('orderby');
    $tab_active = 'all';

    $query = db_select('shop_entity', 'se')
      ->fields('se')
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit(12)
      ->orderBy('se.created', 'DESC');

    if($orderby){
      switch ($orderby) {
        case 'new':
          $query->condition('se.product_type', 0);
          $tab_active = 'new';
          break;
        case 'hot':
          $query->condition('se.product_type', 1);
          $tab_active = 'hot';
          break;
        case 'special':
          $query->condition('se.product_type', 2);
          $tab_active = 'special';
          break;
        default:
      }
    }

    $products = $query->execute()->fetchAll();

    foreach ($products as $key => $p) {
      if ($p->id) {
        $pimgs = db_select('shop_entity__product_images', 'sepi')
        ->fields('sepi', array('product_images_target_id'))
        ->condition('entity_id', $p->id)
        ->execute()
        ->fetchAll();

        foreach ($pimgs as $i => $pi) {
          $products[$key]->product_image[$i] = file_create_url(File::load($pi->product_images_target_id)->getFileUri());
        }
      }
    }

    $pager = '';
    $build['pager'] = array(
      '#type' => 'pager',
      '#weight' => 10,
      '#tags' => array('第一页', '上一页', '', '下一页', '最后一页'),
    );

    $pager_out = drupal_render($build);
    if(is_object($pager_out)){
      $pager = $pager_out->__toString();
    }

    return view('shop', array('products' => $products, 'pager' => $pager, 'tab_active' => $tab_active));
  }

  /**
   * {@inheritdoc}
   */
  public function category($cid) {
    $products = db_select('shop_entity', 'se')
      ->fields('se')
      ->condition('product_cat', $cid)
      ->execute()
      ->fetchAll();


    foreach ($products as $key => $p) {
        if ($p->id) {
          $pimgs = db_select('shop_entity__product_images', 'sepi')
          ->fields('sepi', array('product_images_target_id'))
          ->condition('entity_id', $p->id)
          ->execute()
          ->fetchAll();

          foreach ($pimgs as $i => $pi) {
            $products[$key]->product_image[$i] = file_create_url(File::load($pi->product_images_target_id)->getFileUri());
          }
        }
    }

    return view('shop', array('products' => $products));
  }

  /**
   * {@inheritdoc}
   */
  public function detail($pid) {
    $product = db_select('shop_entity', 'se')
      ->fields('se')
      ->condition('id', $pid)
      ->execute()
      ->fetchAssoc();

    if ($product['id']) {
      $pimgs = db_select('shop_entity__product_images', 'sepi')
      ->fields('sepi', array('product_images_target_id'))
      ->condition('entity_id', $product['id'])
      ->execute()
      ->fetchAll();

      foreach ($pimgs as $i => $pi) {
        $product['product_image'][$i] = file_create_url(File::load($pi->product_images_target_id)->getFileUri());
      }
    }

    if ($product['product_cat']) {
      $product['catname'] = get_catname_bytid($product['product_cat']);
    }

    $same_products = get_samecat_product($product);

    return view('shop_detail', array('product' => $product, 'same_products' => $same_products));
  }

  /**
   * {@inheritdoc}
   */
  public function buy(Request $request) {
    $parms = array();
    if (\Drupal::currentUser()->isAnonymous()) {
      $response = new RedirectResponse("/login");
      $response->send();
      return;
    }

    $parms['pid'] = 0;
    $parms['pcount'] = 0;

    if($request->query->all()){
      $parms['pid'] = $request->query->get('pid');
      $parms['pcount'] = $request->query->get('pcount');
    }

    return view('buy', array('parms' => $parms));
  }

  /**
   * {@inheritdoc}
   */
  public function checkout(Request $request) {
    if (\Drupal::currentUser()->isAnonymous()) {
      $response = new RedirectResponse("/login");
      $response->send();
      return;
    }

    $data['totalMoney'] = $request->request->get('totalMoney');
    $data['products'] = $request->request->get('products');
    $data['orderid'] = $request->request->get('orderid');

    return view('checkout', array('data' => $data));
  }

  /**
   * {@inheritdoc}
   */
  public function order_confirm(Request $request) {
    if (\Drupal::currentUser()->isAnonymous()) {
      $response = new RedirectResponse("/login");
      $response->send();
      return;
    }

    $mailer = new PHPMailer();

    $name = $request->request->get('name');
    $tel = $request->request->get('tel');
    $address = $request->request->get('address');
    $message = $request->request->get('message');

    $orderid = $request->request->get('orderid');
    $total_fee = $request->request->get('total_fee');
    $products = $request->request->get('products');
    $pid = $request->request->get('pid');

    $emailinfo = "<p>逗逼，$name 刚刚在你的小店下单了，赶紧给人家发货：</p>
    <p>订单号：$orderid</p><p> 商品: ";
    foreach ($products as $p) {
      $emailinfo .= $p."<br />";
    }
    $emailinfo .= "</p>";

    $shouhuo_info = "<p> 收货地址： $address</p>
    <p>电话：$tel</p>
    <p>备注：$message</p>
    <p>付费：$total_fee</p>";

    $mailer->setCharset('utf-8');
    $mailer->setEncoding('base64');
    $mailer->setHost('smtp.163.com', 25);
    $mailer->setAuth('aviggngyv', 'xxxxxxx');
    $mailer->setFrom('aviggngyv@163.com', $name);
    $mailer->setTo('498023235@qq.com');
    $mailer->setSubject('新订单:'.$name);
    $mailer->setBody($emailinfo.$shouhuo_info);
    $mailer->isHTML(true);

    if($mailer->send()){
      $oid = db_insert('order_entity')
        ->fields(array(
          'uuid' => $orderid,//\Drupal::service('uuid')->generate()
          'total_fee' => $total_fee,
          'user_id' => \Drupal::currentUser()->id(),
          'pids' => serialize(array($pid)),
          'shouhuo_info' => $shouhuo_info,
          'status' => 0,
          'langcode' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
        ))
        ->execute();

      cart_clear();

      $data['message'] = '你的订单请求已经发送给猎人，请完成支付后，我们将尽快为您发货!';
    }else{
      $data['message'] = '你的订单请求发送失败，请联系猎人!';
    }

    return view('pay', array('data' => $data));
  }

  /**
   * {@inheritdoc}
   */
  public function api_get_product_byid(Request $request) {
    $pid = $request->query->get('pid');
    $pcount = $request->query->get('pcount');

    $product = db_select('shop_entity', 'se')
      ->fields('se')
      ->condition('id', $pid)
      ->execute()
      ->fetchObject();

    if ($product->id) {
      $pimgs = db_select('shop_entity__product_images', 'sepi')
      ->fields('sepi', array('product_images_target_id'))
      ->condition('entity_id', $product->id)
      ->execute()
      ->fetchAll();

      foreach ($pimgs as $i => $pi) {
        $product->product_image[$i] = file_create_url(File::load($pi->product_images_target_id)->getFileUri());
      }
    }

    $product->pcount = $pcount;

    return new JsonResponse($product);
  }

  /**
   * {@inheritdoc}
   */
  public function order_del($oid) {
    $del = db_delete('order_entity')
      ->condition('uuid', $oid)
      ->execute();

    if($del){
      return new JsonResponse(true);
    }

    return new JsonResponse(false);
  }

  /**
   * {@inheritdoc}
   */
  public function login(Request $request) {
    if($request->request->get('name') && $request->request->get('pass')){
      $userAuth = \Drupal::service('user.auth');
      $uid = $userAuth->authenticate($request->request->get('name'), $request->request->get('pass'));

      if($uid){
        $account = User::load($uid);
        user_login_finalize($account);

        $response = new RedirectResponse("/");
        $response->send();
      }else {
        $form = \Drupal::formBuilder()->getForm('Drupal\user\Form\UserLoginForm');

        return view('login', array('form_token' => $form['#token'], 'form_build_id' => $form['#build_id'], 'message' => '帐号或密码错误'));
      }
    }

    if (\Drupal::currentUser()->isAnonymous()) {
      $form = \Drupal::formBuilder()->getForm('Drupal\user\Form\UserLoginForm');

      return view('login', array('form_token' => $form['#token'], 'form_build_id' => $form['#build_id']));
    }

    return;
  }

  /**
   * {@inheritdoc}
   */
  public function regist(Request $request) {
    if($request->request->get('mail') && $request->request->get('name') && $request->request->get('pass')){
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $user = \Drupal\user\Entity\User::create();

      //Mandatory settings
      $user->setPassword($request->request->get('pass'));
      $user->enforceIsNew();
      $user->setEmail($request->request->get('mail'));
      $user->setUsername($request->request->get('name'));//This username must be unique and accept only a-Z,0-9, - _ @ .

      //Optional settings
      $user->set("init", $request->request->get('mail'));
      $user->set("langcode", $language);
      $user->set("preferred_langcode", $language);
      $user->set("preferred_admin_langcode", $language);
      //$user->set("setting_name", 'setting_value');
      $user->activate();

      //Save user
      $user->save();

      //$reguser = \Drupal\user\Entity\User::load($user->id());print_r($reguser);die;
      user_login_finalize($user);

      $response = new RedirectResponse("/");
      $response->send();
    }

    $entity = \Drupal::entityTypeManager()->getStorage('user')->create(array());
    $formObject = \Drupal::entityTypeManager()
      ->getFormObject('user', 'register')
      ->setEntity($entity);
    $form = \Drupal::formBuilder()->getForm($formObject);
    //$outform = \Drupal::service('renderer')->render($form);print_r($outform);die;


    return view('regist', array('form_build_id' => $form['#build_id']));
  }

  /**
   * {@inheritdoc}
   */
  public function user_page($uid = 0) {
    if(!$uid){
      $uid = \Drupal::currentUser()->id();
    }

    $orders = db_select('order_entity', 'oe')
      ->fields('oe')
      ->condition('user_id', $uid)
      ->execute()
      ->fetchAll();

      foreach ($orders as $i => $o) {
        if(!empty(unserialize($o->pids))){
          foreach (unserialize($o->pids) as $value) {
            $orders[$i]->products[] = get_product_bypid($value);
          }
        }
      }

    return view('user', array('uid' => $uid, 'orders' => $orders));
  }

  /**
   * {@inheritdoc}
   */
  public function addToCart($pid) {
    if (\Drupal::currentUser()->isAnonymous()) {
      return new JsonResponse(false);
    }

    $uid = \Drupal::currentUser()->id();
    $pinfo = get_product_bypid($pid);

    if($pinfo){
      $product = new Product($pid, $pinfo['name'], $pinfo['product_price'], [$pinfo['product_cat']]);

      if(!isset($_SESSION['user_'.$uid]['cart'])){
        $cart = new Cart();
      }else{
        $cart = $_SESSION['user_'.$uid]['cart'];
      }

      $cart->add($product);
      $_SESSION['user_'.$uid]['cart'] = $cart;

      return new JsonResponse(true);
    }

    return new JsonResponse(false);
  }

  /**
   * {@inheritdoc}
   */
  public function getCart() {
    $uid = \Drupal::currentUser()->id();
    $cart = $_SESSION['user_'.$uid]['cart'];
    $iterator = $cart->getIterator();
    $products = array();
    $i = 0;

    while ($iterator->valid()) {
        $curr = $iterator->current();
        $products[$i]['id'] = $curr->getId();
        $products[$i]['name'] = $curr->getName();
        $products[$i]['product_price'] = $curr->getPrice();
        $products[$i]['pcount'] = $cart->getTotalOfAProduct($curr);

        if ($curr->getId()) {
          $pimgs = db_select('shop_entity__product_images', 'sepi')
          ->fields('sepi', array('product_images_target_id'))
          ->condition('entity_id', $curr->getId())
          ->execute()
          ->fetchAll();

          foreach ($pimgs as $k => $pi) {
            $products[$i]['product_image'][$k] = file_create_url(File::load($pi->product_images_target_id)->getFileUri());
          }
        }

        $i++;
        $iterator->next();
    }

    return new JsonResponse($products);
  }

}
