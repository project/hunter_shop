<?php

namespace Drupal\hunter_shop;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Order entity entities.
 *
 * @ingroup hunter_shop
 */
interface OrderEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Order entity name.
   *
   * @return string
   *   Name of the Order entity.
   */
  public function getName();

  /**
   * Sets the Order entity name.
   *
   * @param string $name
   *   The Order entity name.
   *
   * @return \Drupal\hunter_shop\OrderEntityInterface
   *   The called Order entity entity.
   */
  public function setName($name);

  /**
   * Gets the Order entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Order entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Order entity creation timestamp.
   *
   * @param int $timestamp
   *   The Order entity creation timestamp.
   *
   * @return \Drupal\hunter_shop\OrderEntityInterface
   *   The called Order entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Order entity published status indicator.
   *
   * Unpublished Order entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Order entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Order entity.
   *
   * @param bool $published
   *   TRUE to set this Order entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\hunter_shop\OrderEntityInterface
   *   The called Order entity entity.
   */
  public function setPublished($published);

}
