<?php

namespace Drupal\hunter_shop;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Order entity entity.
 *
 * @see \Drupal\hunter_shop\Entity\OrderEntity.
 */
class OrderEntityAccessControlHandler extends EntityAccessControlHandler {
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\hunter_shop\OrderEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished order entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published order entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit order entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete order entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add order entity entities');
  }

}
