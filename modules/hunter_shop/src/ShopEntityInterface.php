<?php

namespace Drupal\hunter_shop;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Shop entity entities.
 *
 * @ingroup hunter_shop
 */
interface ShopEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {
  // Add get/set methods for your configuration properties here.
  /**
   * Gets the Shop entity name.
   *
   * @return string
   *   Name of the Shop entity.
   */
  public function getName();

  /**
   * Sets the Shop entity name.
   *
   * @param string $name
   *   The Shop entity name.
   *
   * @return \Drupal\hunter_shop\ShopEntityInterface
   *   The called Shop entity entity.
   */
  public function setName($name);

  /**
   * Gets the Shop entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Shop entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Shop entity creation timestamp.
   *
   * @param int $timestamp
   *   The Shop entity creation timestamp.
   *
   * @return \Drupal\hunter_shop\ShopEntityInterface
   *   The called Shop entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Shop entity published status indicator.
   *
   * Unpublished Shop entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Shop entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Shop entity.
   *
   * @param bool $published
   *   TRUE to set this Shop entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\hunter_shop\ShopEntityInterface
   *   The called Shop entity entity.
   */
  public function setPublished($published);

}
