<?php

namespace Drupal\hunter_shop\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Order entity entities.
 */
class OrderEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['order_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Order entity'),
      'help' => $this->t('The Order entity ID.'),
    );

    return $data;
  }

}
