<?php

namespace Drupal\hunter_shop\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Shop entity entities.
 */
class ShopEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['shop_entity']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Shop entity'),
      'help' => $this->t('The Shop entity ID.'),
    );

    return $data;
  }

}
