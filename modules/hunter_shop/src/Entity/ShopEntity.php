<?php

namespace Drupal\hunter_shop\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\hunter_shop\ShopEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Shop entity entity.
 *
 * @ingroup hunter_shop
 *
 * @ContentEntityType(
 *   id = "shop_entity",
 *   label = @Translation("Shop entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\hunter_shop\ShopEntityListBuilder",
 *     "views_data" = "Drupal\hunter_shop\Entity\ShopEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\hunter_shop\Form\ShopEntityForm",
 *       "add" = "Drupal\hunter_shop\Form\ShopEntityForm",
 *       "edit" = "Drupal\hunter_shop\Form\ShopEntityForm",
 *       "delete" = "Drupal\hunter_shop\Form\ShopEntityDeleteForm",
 *     },
 *     "access" = "Drupal\hunter_shop\ShopEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\hunter_shop\ShopEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "shop_entity",
 *   admin_permission = "administer shop entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/shop_entity/{shop_entity}",
 *     "add-form" = "/admin/structure/shop_entity/add",
 *     "edit-form" = "/admin/structure/shop_entity/{shop_entity}/edit",
 *     "delete-form" = "/admin/structure/shop_entity/{shop_entity}/delete",
 *     "collection" = "/admin/structure/shop_entity",
 *   },
 *   field_ui_base_route = "shop_entity.settings"
 * )
 */
class ShopEntity extends ContentEntityBase implements ShopEntityInterface {
  use EntityChangedTrait;
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? NODE_PUBLISHED : NODE_NOT_PUBLISHED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Shop entity entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Shop entity entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Shop entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Shop entity entity.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // New field being added
    $fields['product_cat'] = BaseFieldDefinition::create('entity_reference')
    ->setSetting('target_type', 'taxonomy_term')
    ->setSetting('handler', 'default:taxonomy_term')
    ->setSetting('handler_settings', array(
      'target_bundles' => array(
        'product_category' => 'product_category'
      )
    ))
    ->setDisplayOptions('view', array(
      'label' => 'hidden',
      'type' => 'author',
      'weight' => 0,
    ))
    ->setDisplayOptions('form', array(
      'type' => 'options_select',
      'weight' => 3,
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'size' => '10',
        'autocomplete_type' => 'tags',
        'placeholder' => '',
      ),
    ))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields["product_images"] = BaseFieldDefinition::create("image")
    ->setLabel(t('Images'))
    ->setDescription(t('Image field'))
    ->setSettings([
      'file_directory' => 'product_images',
      'alt_field_required' => FALSE,
      'file_extensions' => 'png jpg jpeg',
    ])
    ->setDisplayOptions('view', array(
      'label' => 'Product Images',
      'weight' => 0,
    ))
    ->setDisplayOptions('form', array(
      'label' => 'hidden',
      'type' => 'image_image',
      'weight' => 0,
    ))
    ->setCardinality(5)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields["product_price"] = BaseFieldDefinition::create("string")
      ->setLabel(t("Price"))
      ->setDescription(t("The price of the product"))
      ->setSettings(["max_length" => 255, "text_processing" => 0])
      ->setDefaultValue("")
      ->setDisplayOptions("view", ["label" => "above", "type" => "string", "wegith" => -3])
      ->setDisplayOptions("form", ["type" => "string_textfield", "wegith" => -3]);

    $fields["product_description"] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('Please enter a basic objective description about this Point of Interest.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 1,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textarea_with_summary',
        'weight' => 1,
      ));

    $fields['product_type'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('product type'))
      ->setDescription(t('The type of product.'))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE)
      ->setSetting('allowed_values', array('0' => '新品', '1' => '热销', '2' => '特色'))
      ->setDisplayOptions('form', array(
        'type' => 'options_select',
        'weight' => -2,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Shop entity is published.'))
      ->setDefaultValue(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Shop entity entity.'))
      ->setDisplayOptions('form', array(
        'type' => 'language_select',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
