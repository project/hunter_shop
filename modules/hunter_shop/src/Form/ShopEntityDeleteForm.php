<?php

namespace Drupal\hunter_shop\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Shop entity entities.
 *
 * @ingroup hunter_shop
 */
class ShopEntityDeleteForm extends ContentEntityDeleteForm {

}
