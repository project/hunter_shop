<?php

namespace Drupal\hunter_shop\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Order entity entities.
 *
 * @ingroup hunter_shop
 */
class OrderEntityDeleteForm extends ContentEntityDeleteForm {

}
